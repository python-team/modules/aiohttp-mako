ipdb==0.12
coverage==4.5.3
pytest==4.5.0
pytest-cov==2.7.1
pep8==1.7.1
pyflakes==2.1.1
aiohttp==3.5.4
pytest-aiohttp==0.3.0
pytest-sugar==0.9.2
yarl==1.3.0
-e .
